Name:           j4-dmenu-desktop
Version:        r2.18
Release:        1%{?dist}
Summary:        j4-dmenu-desktop is a replacement for i3-dmenu-desktop

License:        GPLv3
URL:            https://www.j4tools.org/
Source0:        https://github.com/enkore/%{name}/archive/%{version}.tar.gz
BuildRoot:      /tmp

BuildRequires:  gcc >= 4.77
BuildRequires:  gcc, cmake, gcc-c++, git
Requires:       dmenu

%description


%prep
%autosetup -n %{name}-%{version}


%build
%cmake . -B .
%{make_build} %{?_smp_mflags}


%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
make install DESTDIR=%{buildroot}

%check
ctest -V %{?_smp_mflags}


%files
%{_bindir}/%{name}

%clean
rm -rf %{buildroot}

%changelog
* Fri Oct 02 2020 Matyáš Kroupa <kroupa.matyas@gmail.com> - r2.18-1
- new version

* Tue May 10 2016 makerpm
- 
